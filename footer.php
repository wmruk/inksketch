
<!-- Footer -->
<footer class="page-footer font-small mdb-color pt-4 bg-dark">

    <!-- Footer Links -->
    <div class="container text-center text-md-left">
        <!--                 Footer links  -->
        <div class="row text-center text-md-left mt-3 pb-3">
            <?php if (is_active_sidebar('footer1')) : ?>
                <?php dynamic_sidebar('footer1'); ?>
            <?php endif; ?>

            <?php if (is_active_sidebar('footer2')) : ?>
                <?php dynamic_sidebar('footer2'); ?>
            <?php endif; ?>

            <?php if (is_active_sidebar('footer4')) : ?>
                <?php dynamic_sidebar('footer4'); ?>
            <?php endif; ?>

            <?php // if (is_active_sidebar('footer4')) : ?>
                <?php // dynamic_sidebar('footer4'); ?>
            <?php // endif; ?>

        </div>


        <hr>

        <!-- Grid row -->
        <div class="row d-flex align-items-center">

            <!-- Grid column -->
            <div class="col-md-7 col-lg-8">
                <?php if (is_active_sidebar('footer')) : ?>
                    <?php dynamic_sidebar('footer'); ?>
                <?php endif; ?>
                <!--Copyright-->

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-5 col-lg-4 ml-lg-0">

                <?php if (is_active_sidebar('footersocials')) : ?>
                    <?php dynamic_sidebar('footersocials'); ?>
                <?php endif; ?>

                <!--                         Social buttons 
                                        <div class="text-center text-md-right">
                                            <ul class="list-unstyled list-inline">
                                                <li class="list-inline-item">
                                                    <a class="btn-floating btn-sm rgba-white-slight mx-1">
                                                        <i class="fab fa-facebook-f"></i>
                                                    </a>
                                                </li>
                                                <li class="list-inline-item">
                                                    <a class="btn-floating btn-sm rgba-white-slight mx-1">
                                                        <i class="fab fa-twitter"></i>
                                                    </a>
                                                </li>
                                                <li class="list-inline-item">
                                                    <a class="btn-floating btn-sm rgba-white-slight mx-1">
                                                        <i class="fab fa-google-plus"></i>
                                                    </a>
                                                </li>
                                                <li class="list-inline-item">
                                                    <a class="btn-floating btn-sm rgba-white-slight mx-1">
                                                        <i class="fab fa-instagram"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>-->

            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->

    </div>
    <!-- Footer Links -->

</footer>
<?php wp_footer(); ?>
<script src="<?php bloginfo('template_url'); ?>/js/bootstrap.bundle.min.js"></script>

</body>

</html>

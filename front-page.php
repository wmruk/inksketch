<?php get_header(); ?>
<div class="container">
     <?php  dynamic_sidebar('smartslider_area_1'); ?>
    <div class="row">
        <div class="col-sm-8">
            <?php if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>
                    <!--<div class="blog-post">-->
                    <?php the_content(); ?>
                    <!--</div> /.blog-post -->
                <?php endwhile; ?>
            <?php else : ?>
                <p><?php __('No Page Found'); ?></p>
            <?php endif; ?>
        </div>

        <div class="col-sm-4 centered">
            <?php if (is_active_sidebar('getintouch')) : ?>
                <?php dynamic_sidebar('getintouch'); ?>
            <?php endif; ?>
        </div>
    </div>
    <!-- /.row -->
    <div class="row">

        <div class="col-md-4">
            <div class="card">
                <?php if (is_active_sidebar('box1')) : ?>
                    <?php dynamic_sidebar('box1'); ?>
                <?php endif; ?>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card">
                <?php if (is_active_sidebar('box2')) : ?>
                    <?php dynamic_sidebar('box2'); ?>
                <?php endif; ?>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card">
                <?php if (is_active_sidebar('box3')) : ?>
                    <?php dynamic_sidebar('box3'); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <!-- /.row -->

</div>
<!-- /.container -->
<?php get_footer(); ?>
<?php

// Register Nav Walker class_alias
require_once('wp_bootstrap_navwalker.php');

// Theme Support
function wpb_theme_setup() {
    add_theme_support('post-thumbnails');

    // Nav Menus
    register_nav_menus(array(
        'primary' => __('Primary Menu')
    ));

    // Post Formats
    add_theme_support('post-formats', array('aside', 'gallery'));
}

add_action('after_setup_theme', 'wpb_theme_setup');

// custom logo

add_theme_support('custom-logo');

function themename_custom_logo_setup() {
    $defaults = array(
        'height' => 100,
        'width' => 400,
        'flex-height' => true,
        'flex-width' => true,
        'header-text' => array('site-title', 'site-description'),
    );
    add_theme_support('custom-logo', $defaults);
}

add_action('after_setup_theme', 'themename_custom_logo_setup');

// Widget Locations
function wpb_init_widgets($id) {
    register_sidebar(array(
        'name' => 'Sidebar',
        'id' => 'sidebar',
        'before_widget' => '<div class="sidebar-module">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ));

    register_sidebar(array(
        'name' => 'GetInTouch',
        'id' => 'getintouch',
        'before_widget' => '<address>',
        'after_widget' => '</address>',
        'before_title' => '<h2 class="mt-4">',
        'after_title' => '</h2>'
    ));

    register_sidebar(array(
        'name' => 'footer',
        'id' => 'footer',
        'before_widget' => '<div class="col-md-7 col-lg-8">',
        'after_widget' => '</div>',
    ));

    register_sidebar(array(
        'name' => 'Box1',
        'id' => 'box1',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
        'before_widget' => '<div class="card">',
        'after_widget' => '</div>',
    ));

    register_sidebar(array(
        'name' => 'Box2',
        'id' => 'box2',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
        'before_widget' => '<div class="card">',
        'after_widget' => '</div>',
    ));

    register_sidebar(array(
        'name' => 'Box3',
        'id' => 'box3',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
        'before_widget' => '<div class="card">',
        'after_widget' => '</div>',
    ));

    for ($i = 1; $i <= 4; $i ++) {
        register_sidebar(array(
            'name' => 'Footer' . $i,
            'id' => 'footer' . $i,
            'before_title' => '<h6>',
            'after_title' => '</h6>',
            'before_widget' => '<div class="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3" >',
            'after_widget' => '</div>',
        ));
    }

    
    for ($i = 1; $i <= 9; $i ++) {
        register_sidebar(array(
            'name' => 'Portfolio_' . $i,
            'id' => 'portfolio_' . $i,
//            'before_title' => '<h6>',
//            'after_title' => '</h6>',
            'before_widget' => '<div class="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3" >',
            'after_widget' => '</div>',
        ));
    }
    
    register_sidebar(array(
        'name' => 'ContactFormLeft',
        'id' => 'contactformleft',
        'before_widget' => '<div class="col-md-6 centered">',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>'
    ));

    register_sidebar(array(
        'name' => 'ContactFormRight',
        'id' => 'contactformright',
        'before_widget' => '<div class="col-md-6 text-right">',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>'
    ));

    register_sidebar(array(
        'name' => 'FooterSocials',
        'id' => 'footersocials',
        'before_title' => '<h6>',
        'after_title' => '</h6>',
        'before_widget' => '<div class="text-center text-md-right">',
        'after_widget' => '</div>',
    ));
}

add_action('widgets_init', 'wpb_init_widgets');

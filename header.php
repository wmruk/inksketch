<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="<?php bloginfo('description'); ?>">

        <title>
            <?php bloginfo('name'); ?> | 
            <?php is_front_page() ? bloginfo('description') : wp_title(); ?>
        </title>
        
        <link href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php bloginfo('template_url'); ?>/css/business-frontpage.css" rel="stylesheet">
        <link href="<?php bloginfo('template_url'); ?>/style.css" rel="stylesheet">
        <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">-->
        <link href="<?php bloginfo('template_url'); ?>/css/font_awsome/css/font-awesome.min.css" rel="stylesheet">
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.min.js"></script>
      <?php wp_head(); ?>
  </head>

    <body>

        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
            <div class="container">
                <a class="navbar-brand" href="/">
                    <?php
                    $custom_logo_id = get_theme_mod('custom_logo');
                    $logo = wp_get_attachment_image_src($custom_logo_id, 'full');
                    if (has_custom_logo()) {
                        echo '<img id="logo" src="' . esc_url($logo[0]) . '">';
                    } else {
                        echo '<h1>' . get_bloginfo('name') . '</h1>';
                    }
                    ?>

                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarResponsive">
  
                        <?php
                        wp_nav_menu(array(
                            'menu' => 'main-menu',
                            'menu_item' => 'abc',
                            'theme_location' => 'main-menu',
                            'depth' => 2,
                            'container' => '',
                            'container_class' => 'container_class',
                            'container_id' => 'container_id',
                            'menu_class' => 'navbar-nav ml-auto',
                            'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                            'walker' => new wp_bootstrap_navwalker())
                        );
                        ?>
                </div>
            </div>
        </nav>
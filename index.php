<?php get_header(); ?>
<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-sm-8">
            <?php if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>
                    <div class="blog-post">
                        <h2 class="blog-post-title">
                            <?php the_title(); ?>
                        </h2>

                        <?php the_content(); ?>
                    </div><!-- /.blog-post -->
                <?php endwhile; ?>
            <?php else : ?>
                <p><?php __('No Page Found'); ?></p>
            <?php endif; ?>
        </div>
        <div class="col-sm-4 centered">

            <h2 class="mt-4">Get in touch</h2>

            <address>
                <strong>Mikolaj Filasinski</strong>
                <br>Close Road 11a
                <br>Chrząszczyżewoszyce, powiat Łękołody
                <br>
            </address>
            <address>
                <abbr title="Phone">P:</abbr>
                (+48) 123-4567
                <br>
                <abbr title="Email">E:</abbr>
                <a href="mailto:#">name@example.com</a>
            </address>

        </div>
    </div>
    <!-- /.row -->
    <h2 class="mt-4">Most Wanted</h2>
    <div class="row">
        <div class="col-sm-4 my-4">
            <div class="card">
                <img class="card-img-top" src="<?php bloginfo('template_url'); ?>/img/wanted_1.jpg" alt="">
                <div class="card-body">
                    <h4 class="card-title">Watercolors</h4>
                    <!--<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente esse necessitatibus neque sequi doloribus.</p>-->
                </div>
                <div class="card-footer">
                    <a href="#" class="btn btn-light">Find Out More!</a>
                </div>
            </div>
        </div>
        <div class="col-sm-4 my-4">
            <div class="card">
                <img class="card-img-top" src="<?php bloginfo('template_url'); ?>/img/wanted_2.jpg" alt="">
                <div class="card-body">
                    <h4 class="card-title">Ink</h4>
                    <!--<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente esse necessitatibus neque sequi doloribus totam ut praesentium aut.</p>-->
                </div>
                <div class="card-footer">
                    <a href="#" class="btn btn-light">Find Out More!</a>
                </div>
            </div>
        </div>
        <div class="col-sm-4 my-4">
            <div class="card">
                <img class="card-img-top" src="<?php bloginfo('template_url'); ?>/img/wanted_3.jpg" alt="">
                <div class="card-body">
                    <h4 class="card-title">Pencil</h4>
                    <!--<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente esse necessitatibus neque.</p>-->
                </div>
                <div class="card-footer">
                    <a href="#" class="btn btn-light">Find Out More!</a>
                </div>
            </div>
        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->
<?php get_footer(); ?>
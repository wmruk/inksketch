<?php get_header(); ?>
<div class="container  h-100">
    <div class="row">
        <div class="col-sm-12">
            <?php if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>
                    <!--<div class="blog-post">-->
                    <?php the_content(); ?>
                    <!--</div> /.blog-post -->
                <?php endwhile; ?>
            <?php else : ?>
                <p><?php __('No Page Found'); ?></p>
            <?php endif; ?>
        </div>
    </div>
    <div class="container h-100">
      <div class="row h-100 justify-content-center align-items-center">
                <?php if (is_active_sidebar('contactformleft')) : ?>
                    <?php dynamic_sidebar('contactformleft'); ?>
                <?php endif; ?>

                <?php if (is_active_sidebar('contactformright')) : ?>
                    <?php dynamic_sidebar('contactformright'); ?>
                <?php endif; ?>
      </div>
    </div>
</div>




<!-- /.container -->
<?php get_footer(); ?>
<?php get_header(); ?>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <?php if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>
                    <!--<div class="blog-post">-->
                    <?php the_content(); ?>
                    <!--</div> /.blog-post -->
                <?php endwhile; ?>
            <?php else : ?>
                <p><?php __('No Page Found'); ?></p>
            <?php endif; ?>
        </div>
    </div>
   </div>
<!-- /.container -->
<?php get_footer(); ?>